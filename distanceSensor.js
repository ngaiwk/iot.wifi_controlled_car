
const controlMap = require("./control.js");

const Gpio = require('pigpio').Gpio;
const trigger = new Gpio(5, { mode: Gpio.OUTPUT });
const echo = new Gpio(6, { mode: Gpio.INPUT, alert: true });
const buzzer = new Gpio(25, { mode: Gpio.OUTPUT });
const MICROSECDONDS_PER_CM = 1e6 / 34321;

trigger.digitalWrite(0); // Make sure trigger is low 

let alert = false;

function checkDistance() {
    let startTick;
    echo.on('alert', (level, tick) => {
        if (level == 1) {
            startTick = tick;
        } else {
            const endTick = tick;
            const diff = (endTick >> 0) - (startTick >> 0); // Unsigned 32 bit arithmetic
            distance = diff / 2 / MICROSECDONDS_PER_CM;
            //in cm distance
            if (distance < 10) {
                buzzer.digitalWrite(1);
                // trigger stop
                controlMap(0, 0);
                alert = true;
            }
            else if (alert) {
                alert = false;
                buzzer.digitalWrite(0);
            }
        }
    });

    // Trigger a distance measurement once per second
    setInterval(() => {
        trigger.trigger(10, 1); // Set trigger high for 10 microseconds
    }, 250);

};



module.exports = { checkDistance, buzzer };

