const dotenv = require("dotenv");
dotenv.config();


const mqtt = require("mqtt");
const client = mqtt.connect(`mqtt://${process.env.SERVER}:${process.env.PORT}`);

// subscribe
const TOPIC_CONTROL = "car/control/" + process.env.CAR_IDENTIFIER;
const TOPIC_PING = "car/ping/" + process.env.CAR_IDENTIFIER;
// publish
const TOPIC_PONG = "pong";
const TOPIC_DATA = "data";
const TOPIC_STREAM = "stream";

client.subscribe(TOPIC_CONTROL);
client.subscribe(TOPIC_PING);

client.on("connect", function () {
  client.publish("identifier", process.env.CAR_IDENTIFIER);
  console.log("connected");
});



let x = 0;
let y = 0;
client.on("message", function (topic, message) {
  if (topic == TOPIC_CONTROL) {
    const control = JSON.parse(message.toString());
    x = 0;
    y = 0;
    if (control[0]) y++;
    if (control[1]) y--;
    if (control[2]) x--;
    if (control[3]) x++;
    //control [8] is boost function
    controlMap(x, y, control[8]);

    // Camera Control
    if (!control[6]) {
      let servoControl = 0;
      if (control[4]) servoControl++;
      if (control[5]) servoControl--;
      if (servoControl) {
        servo.move(servoControl);
      } else {
        servo.stop();
      }
    } else {
      servo.reset();
    }

    // buzzer Control
    if (control[7]) {
      buzzer.digitalWrite(1);
    } else {
      buzzer.digitalWrite(0);
    }
  } else if (topic == TOPIC_PING) {
    client.publish(TOPIC_PONG);
  }
});

client.on("disconnect", function () {
  client.end();

  clearInterval(camInterval);
  clearInterval(dataInterval);
  stop();
  console.log("disconnected");
});


// imports
const controlMap = require("./control");
const { checkDistance, buzzer } = require("./distanceSensor");
checkDistance();
const servo = require("./servoMotor");


// Webcam
const cv = require("opencv4nodejs");
const cam = new cv.VideoCapture(0);
let camInterval = setInterval(() => {
  if (client.connected) {
    cam.read();
    cam.readAsync((err, frame) => {
      if (!err && !frame.empty) {
        client.publish(TOPIC_STREAM, cv.imencode(".jpg", frame).toString("base64"));
      }
    });
  }
}, Math.round(1000 / Number(process.env.FPS)));


// Data Interval
let temp = require("pi-temperature");
let dhtSensor = require("node-dht-sensor");
let dataInterval = setInterval(() => {

  if (client.connected) {
    // CPU Temperature
    temp.measure(function (err, temp) {
      if (!err) {
        client.publish(TOPIC_DATA, JSON.stringify({ cpuTemp: temp }));
      }
    });

    // Room Temperature and humidity
    dhtSensor.read(11, 4, function (err, temperature, humidity) {
      if (!err) {
        client.publish(TOPIC_DATA, JSON.stringify({ roomTemp: temperature, roomHumi: humidity }));
      }
    });
  }
}, 5000);

module.exports = { controlMap };

