
function controlMap(x, y, boost) {
    if (y == 1 && x == 0 && boost == false) {
        forward();
    }
    else if (y == -1 && x == 0 && boost == false) {
        backward();
    }
    else if (y == 0 && x == -1 && boost == false) {
        left();
    }
    else if (y == 0 && x == 1 && boost == false) {
        right();
    }
    else if (y == 1 && x == -1 && boost == false) {
        leftForward();
    }
    else if (y == 1 && x == 1 && boost == false) {
        rightForward();
    }
    else if (y == -1 && x == -1 && boost == false) {
        leftBackward();
    }
    else if (y == -1 && x == 1 && boost == false) {
        rightBackward();
    }
    else if (y == 1 && x == 0 && boost == true) {
        forwardBoost();
        // console.log("go boost forward");
    }
    else if (y == -1 && x == 0 && boost == true) {
        backwardBoost();
        // console.log("go boost backward");
    }
    else {
        // console.log("go stop");
        stop();
    }
}


const Gpio = require('pigpio').Gpio;
//========with 9v tx
//right
const rightMotorSpeed = new Gpio(13, { mode: Gpio.OUTPUT });
const rightMotorForward = new Gpio(26, { mode: Gpio.OUTPUT });
const rightMotorBackward = new Gpio(19, { mode: Gpio.OUTPUT });
//left
const leftMotorSpeed = new Gpio(16, { mode: Gpio.OUTPUT });
const leftMotorForward = new Gpio(21, { mode: Gpio.OUTPUT });
const leftMotorBackward = new Gpio(20, { mode: Gpio.OUTPUT });

function forward() {
    // console.log("testing forward");
    rightMotorSpeed.analogWrite(127);
    rightMotorForward.digitalWrite(1);
    rightMotorBackward.digitalWrite(0);
    leftMotorSpeed.analogWrite(127);
    leftMotorForward.digitalWrite(1);
    leftMotorBackward.digitalWrite(0);
}

function backward() {
    // console.log("testing backward");
    rightMotorSpeed.analogWrite(127);
    rightMotorForward.digitalWrite(0);
    rightMotorBackward.digitalWrite(1);
    leftMotorSpeed.analogWrite(127);
    leftMotorForward.digitalWrite(0);
    leftMotorBackward.digitalWrite(1);
}

function stop() {
    // console.log("testing stop");
    rightMotorSpeed.analogWrite(0);
    rightMotorForward.digitalWrite(0);
    rightMotorBackward.digitalWrite(0);
    leftMotorSpeed.analogWrite(0);
    leftMotorForward.digitalWrite(0);
    leftMotorBackward.digitalWrite(0);
}

function left() {
    // console.log("testing left");
    rightMotorSpeed.analogWrite(255);
    rightMotorForward.digitalWrite(1);
    rightMotorBackward.digitalWrite(0);
    leftMotorSpeed.analogWrite(0);
    leftMotorForward.digitalWrite(0);
    leftMotorBackward.digitalWrite(0);
}

function right() {
    // console.log("testing right");
    rightMotorSpeed.analogWrite(0);
    rightMotorForward.digitalWrite(0);
    rightMotorBackward.digitalWrite(0);
    leftMotorSpeed.analogWrite(255);
    leftMotorForward.digitalWrite(1);
    leftMotorBackward.digitalWrite(0);
}

function leftForward() {
    // console.log("testing leftForward");
    rightMotorSpeed.analogWrite(255);
    rightMotorForward.digitalWrite(1);
    rightMotorBackward.digitalWrite(0);
    leftMotorSpeed.analogWrite(127);
    leftMotorForward.digitalWrite(1);
    leftMotorBackward.digitalWrite(0);
}

function rightForward() {
    // console.log("testing rightForward");
    rightMotorSpeed.analogWrite(127);
    rightMotorForward.digitalWrite(1);
    rightMotorBackward.digitalWrite(0);
    leftMotorSpeed.analogWrite(255);
    leftMotorForward.digitalWrite(1);
    leftMotorBackward.digitalWrite(0);
}

function leftBackward() {
    // console.log("testing leftBackward");
    rightMotorSpeed.analogWrite(255);
    rightMotorForward.digitalWrite(0);
    rightMotorBackward.digitalWrite(1);
    leftMotorSpeed.analogWrite(127);
    leftMotorForward.digitalWrite(0);
    leftMotorBackward.digitalWrite(1);
}

function rightBackward() {
    // console.log("testing rightBackward");
    rightMotorSpeed.analogWrite(127);
    rightMotorForward.digitalWrite(0);
    rightMotorBackward.digitalWrite(1);
    leftMotorSpeed.analogWrite(255);
    leftMotorForward.digitalWrite(0);
    leftMotorBackward.digitalWrite(1);
}

function forwardBoost(){
    // console.log("testing boost forward");
    rightMotorSpeed.analogWrite(255);
    rightMotorForward.digitalWrite(1);
    rightMotorBackward.digitalWrite(0);
    leftMotorSpeed.analogWrite(255);
    leftMotorForward.digitalWrite(1);
    leftMotorBackward.digitalWrite(0);
}

function backwardBoost(){
    // console.log("testing boost backward");
    rightMotorSpeed.analogWrite(255);
    rightMotorForward.digitalWrite(0);
    rightMotorBackward.digitalWrite(1);
    leftMotorSpeed.analogWrite(255);
    leftMotorForward.digitalWrite(0);
    leftMotorBackward.digitalWrite(1);
}

module.exports = controlMap;