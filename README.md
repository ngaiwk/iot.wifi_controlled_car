# iot.wifi_controlled_car

This is part of internet of things project, this repository program load in a remote-car (use raspberry pi) after the pi connected with internet by wifi, the car can control by using browser.

<img src ="/uploads/9c1cb4f95dcfd72ee228e426531be2f9/car.png" width="400" height="550">

<img src ="/uploads/1ca35be81451e89a8bfb6f48a4a5694a/car_fin.png" width="400" height="550">

[![remote_car](http://img.youtube.com/vi/Yleedu1GAqM/0.jpg)](https://www.youtube.com/watch?v=Yleedu1GAqM)
<br><youtube>https://www.youtube.com/watch?v=Yleedu1GAqM</youtube>


