const Gpio = require('pigpio').Gpio;

const motor = new Gpio(10, { mode: Gpio.OUTPUT });

let interval;

const MAX = 2000;
const MID = 1500;
const MIN = 1000;
const turnRate = 100;
let pulseWidth = 1500;
let control = 0;

const updateServoMotor = () => {
    pulseWidth += turnRate * control;
    if (pulseWidth > MAX) {
        pulseWidth = MAX;
    } else if (pulseWidth < MIN) {
        pulseWidth = MIN;
    }
    motor.servoWrite(pulseWidth);
}

function move(value) {
    control = value;
    if (!interval || interval._destroyed) {
        interval = setInterval(updateServoMotor, 50);
    }
}

function stop() {
    clearInterval(interval);
}

function reset() {
    clearInterval(interval);
    control = 0;
    pulseWidth = MID;
    updateServoMotor();
}


module.exports = { move, stop, reset };
