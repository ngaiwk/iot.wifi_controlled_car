
// const { controlMap, x } = require("./index");
const {controlMap} = require("./control.js");
const Gpio = require('pigpio').Gpio;
const trigger = new Gpio(5, { mode: Gpio.OUTPUT });
const echo = new Gpio(6, { mode: Gpio.INPUT, alert: true });
const buzzer = new Gpio(25, { mode: Gpio.OUTPUT });
const MICROSECDONDS_PER_CM = 1e6 / 34321;
var userControl = true;

trigger.digitalWrite(0); // Make sure trigger is low 

function checkDistance() {
    let startTick;
    echo.on('alert', (level, tick) => {
        if (level == 1) {
            startTick = tick;
        } else {
            const endTick = tick;
            const diff = (endTick >> 0) - (startTick >> 0); // Unsigned 32 bit arithmetic
            distance = diff / 2 / MICROSECDONDS_PER_CM;
            //in cm distance
            console.log(distance);
            // trigger alret sensor
            // let crashAlert = false;
            if (distance < 10) {
                buzzer.digitalWrite(1);
                //=======crash determination
                if (x == 1 || x == -1) {
                    userControl = false;
                    if (userControl == false) {
                        //call stop
                        controlMap(2, 2);
                    }
                }
                else {
                    console.log("fault alret");
                    userControl = true;
                }
            }
            else {
                userControl = true;
                // console.log(userControl);
                buzzer.digitalWrite(0);
                console.log("drive safe");
            }
        }
    });

    // Trigger a distance measurement once per second
    setInterval(() => {
        trigger.trigger(10, 1); // Set trigger high for 10 microseconds
    }, 500);
};



module.exports = checkDistance;

